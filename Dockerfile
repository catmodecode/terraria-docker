FROM alpine:3.12 as build

ARG VERSION=1449

RUN apk add wget unzip

WORKDIR /server

RUN wget https://terraria.org/api/download/pc-dedicated-server/terraria-server-${VERSION}.zip && \
    unzip terraria-server-${VERSION}.zip && \
    chmod +x ${VERSION}/Linux/TerrariaServer.bin.x86* && \
    chmod +x ${VERSION}/Linux/TerrariaServer

RUN mv /server/${VERSION}/Linux/ /server/Linux

FROM ubuntu:24.10

USER 1000

WORKDIR /server

COPY --from=build --chown=1000:1000 /server/Linux/ /server

EXPOSE 7777/udp 7777

ENTRYPOINT [ "./TerrariaServer", "-config", "server.cfg" ]